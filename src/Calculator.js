import React, { Component } from "react";
import "./Calculator.css";
import Display from "./components/Display/Display";
import Keypad from "./components/Keypad/Keypad";

class Calculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: "",
    };
    // console.log("constructor");
  }

  buttonPressed(buttonName){
    //  console.log("from Calculator",this)
    //   console.log("buttonname",buttonName);
    if (buttonName === "=") {
      this.calculate();
    } else if (buttonName === "C") {
      this.reset();
    } else if (buttonName === "CE") {
      this.backspace();
    } else
      this.setState({
       result: this.state.result + buttonName,
      });
      //console.log(this);
  };
  calculate() {
    try {
      this.setState({
        result: (eval(this.state.result) || "") + "",
      });
     // console.log(this)
    } catch (e) {
      this.setState({
        result: "error",
      });
      //console.log(this)
    }
  }
  backspace() {
    this.setState({
      result: this.state.result.slice(0, -1),
    });
    //console.log(this);
  }
  reset() {
    this.setState({
      result: "",
    });
    //console.log(this);
  }

  render() {
      //console.log("render");
    return (
      <div className="Calculator">
        <h1>Calculator</h1>
        <div className="calc-body">
          <Display result={this.state.result} />
          <Keypad buttonPressed={this.buttonPressed.bind(this)} />
        </div>
      </div>
    );
  }
}

export default Calculator;
